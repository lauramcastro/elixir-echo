defmodule GenEchoSup do
  @moduledoc """
  Documentation for `GenEchoSup`, an echo server supervisor. Supervises the echo server defined in the `GenEcho` module.
  """
  use Supervisor

  @doc """
  Starts the echo server supervisor, which in turns starts the echo server.

  ## Examples

      iex> {:ok, pid} = GenEchoSup.start_link([])
      iex> is_pid(pid)
      true
      iex> GenEcho.echo("Hello, world!")
      "Hello, world!"

  """
  @spec start_link(term()) ::
          {:ok, pid()}
          | :ignore
          | {:error, {:already_started, pid()} | {:shutdown, term()} | term()}
  def start_link(init_arg) do
    Supervisor.start_link(__MODULE__, init_arg, name: __MODULE__)
  end

  @impl true
  def init(_whatever) do
    children = [
      %{
        id: GenEcho,
        start: {GenEcho, :start, []},
        restart: :transient,
        shutdown: 100,
        type: :worker
      }
    ]

    Supervisor.init(children, strategy: :one_for_one, max_restarts: 1000, max_seconds: 1000)
  end
end
