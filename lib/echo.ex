defmodule Echo do
  @moduledoc """
  Documentation for `Echo`, a process that just replies what it receives back.
  """

  @doc """
  Starts a new echo server, returning its identifier.

  ## Examples

      iex> pid = Echo.start()
      iex> is_pid(pid)
      true

  """
  @spec start() :: pid()
  def start() do
    spawn(fn -> loop() end)
  end

  @doc """
  Stops the echo server.

  It does not fail if the server was not started.

  ## Examples

      iex> pid = Echo.start()
      iex> Echo.stop(pid)
      :ok
      iex> Echo.stop(pid)
      :ok

  """
  @spec stop(pid()) :: :ok
  def stop(pid) do
    send(pid, :stop)
    :ok
  end

  @doc """
  Sends a message `message` to the echo process, waits for a response, and returns it.

  ## Examples

      iex> pid = Echo.start()
      iex> Echo.echo(pid, "Hello, world!")
      "Hello, world!"
      iex> Echo.stop(pid)
      :ok

  """
  @spec echo(pid(), term()) :: term()
  def echo(pid, message) do
    send(pid, {:echo, message, self()})

    receive do
      {:echo_reply, message} ->
        message
    end
  end

  ## Private functions
  defp loop() do
    receive do
      :stop ->
        :ok

      {:echo, message, who} ->
        send(who, {:echo_reply, message})
        loop()
    end
  end
end
