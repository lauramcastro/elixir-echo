defmodule GenEchoes do
  @moduledoc """
  Documentation for `GenEchoes`, a multi-instance (supervisable) echo server.

  As improvement over the `GenEcho` implementation, this version allows to start
  more than one echo server, by using auto-generated atoms for process registration.
  It also naturally supports distribution.
  """
  use GenServer

  @doc """
  Starts an echo server, to be registered as `name`.

  ## Examples

      iex> {:ok, pid} = GenEchoes.start(:one_echo)
      iex> is_pid(pid)
      true
      iex> pid == Process.whereis(:one_echo)
      true

  """
  @spec start(atom() | {atom(), node()}) ::
          {:ok, pid()} | :ignore | {:error, {:already_started, pid()} | term()}
  def start(name) do
    GenServer.start_link(__MODULE__, [name], name: name)
  end

  @doc """
  Kills a local echo server! (for testing purposes)

  ## Examples

      iex> Process.flag(:trap_exit, true)
      iex> {:ok, pid} = GenEchoes.start(:echo)
      iex> true = Process.alive?(pid)
      iex> true = GenEchoes.kill(:echo)
      iex> Process.alive?(pid)
      false

  """
  @spec kill(atom() | {atom(), node()}) :: boolean()
  def kill(name), do: Process.whereis(name) |> Process.exit(:test)

  @doc """
  Sends a message `message` to the `name` echo server and returns its reply.

  ## Examples

     iex> GenEchoes.start(:one_echo)
     iex> GenEchoes.echo(:one_echo, "Hello, world!")
     {:one_echo, "Hello, world!"}

  """
  @spec echo(atom(), term()) :: term()
  def echo(name, message) do
    GenServer.call(name, {:echo, message})
  end

  # Callbacks
  @impl true
  def init([name]) do
    {:ok, name}
  end

  @impl true
  def handle_call({:echo, message}, _from, name) do
    {:reply, {name, message}, name}
  end
end
