defmodule GenEchoesSup do
  @moduledoc """
  Documentation for `GenEchoesSup`, a multi-instance echo server supervisor.
  Supervises the echo server instances defined in the `GenEchoes` module.
  """
  use Supervisor

  @doc """
  Starts the multi-instance echo server supervisor, which in turns starts `n` echo servers, named `echo1..echon`.

  ## Examples

      iex> {:ok, pid} = GenEchoesSup.start_link(3)
      iex> is_pid(pid)
      true
      iex> GenEchoes.echo(:echo1, "Hello, world!")
      {:echo1, "Hello, world!"}
      iex> GenEchoes.echo(:echo2, "Hello world, too!")
      {:echo2, "Hello world, too!"}
      iex> GenEchoes.echo(:echo3, "Hello world, three!")
      {:echo3, "Hello world, three!"}

  """
  @spec start_link(non_neg_integer()) ::
          {:ok, pid()}
          | :ignore
          | {:error, {:already_started, pid()} | {:shutdown, term()} | term()}
  def start_link(n) do
    Supervisor.start_link(__MODULE__, [n], name: __MODULE__)
  end

  @doc """
  Stops the multi-instance echo server supervisor, which in turns stops all its echo server children.

  ## Examples

      iex> Process.flag(:trap_exit, true)
      iex> GenEchoesSup.start_link(3)
      iex> pid = Process.whereis(GenEchoesSup)
      iex> true = GenEchoesSup.stop()
      iex> Process.alive?(pid)
      false

  """
  @spec stop() :: true
  def stop() do
    Process.whereis(GenEchoesSup) |> Process.exit(:kill)
  end

  @impl true
  def init([n]) do
    children =
      Enum.map(1..n, fn e ->
        id = String.to_atom("echo" <> to_string(e))

        %{
          id: id,
          start: {GenEchoes, :start, [id]},
          restart: :transient,
          shutdown: 100,
          type: :worker
        }
      end)

    Supervisor.init(children, strategy: :one_for_one, max_restarts: 1000, max_seconds: 1000)
  end
end
