defmodule GenEcho do
  @moduledoc """
  Documentation for `GenEcho`, a (supervisable) echo server. This implementation differs from the `Echo`module in that it uses the `GenServer` behaviour.
  """
  use GenServer

  @doc """
  Starts the echo server.

  ## Examples

      iex> {:ok, pid} = GenEcho.start()
      iex> is_pid(pid)
      true

  """
  @spec start() :: {:ok, pid()} | :ignore | {:error, {:already_started, pid()} | term()}
  def start() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc """
  Sends a message `message` to the echo server and returns its reply.

  ## Examples

     iex> GenEcho.start()
     iex> GenEcho.echo("Hello, world!")
     "Hello, world!"

  """
  @spec echo(term()) :: term()
  def echo(message) do
    GenServer.call(__MODULE__, {:echo, message})
  end

  # Callbacks
  @impl true
  def init(_whatever) do
    {:ok, []}
  end

  @impl true
  def handle_call({:echo, message}, _from, state) do
    {:reply, message, state}
  end
end
