# Elixir Echo

This is the Elixir version of [https://gitlab.com/lauramcastro/erlang-echo](https://gitlab.com/lauramcastro/erlang-echo), thus a demonstrator for Elixir capabilities when implementing fault-tolerant architectures... and testing them!

##  Description

This demonstrator is built as a four-step exercise from a simple echo server to a version with uses OTP behaviours and supports distribution. Diagrams below follow the [C4 representation model](https://c4model.com) for software architecture.

### Echo server

Module `Echo` defines a simple echo server implemented with a custom process. For its validation, the project includes a PBT stateful model `EchoStatefulPBTTest`).

<img src="./doc/Echo.png" alt="simple echo diagram" height="300"/>

### GenServer-based echo server (with supervision)

Module `GenEcho` redefines the `Echo` server, now using the [`GenServer`](https://hexdocs.pm/elixir/GenServer.html) behaviour. Taking advantage of this, a [Supervisor](https://hexdocs.pm/elixir/Supervisor.html) is included (`GenEchoSup`). The corresponding PBT stateful model `GenEchoStatefulPBTTest` is provided.

<img src="./doc/SupervisedEcho.png" alt="gen_server-based echo diagram" height="450"/>

### Multiple-instance echo server

Module `GenEchoes` modifies the previous `GenServer`-based version sligthly to achieve the capability of having multiple instances of `Echo` servers, under the same supervisor (`GenEchoesSup`). Once more, a PBT stateful model `GenEchoesStatefulPBTTest` is also in place.

<img src="./doc/SupervisedEchoes.png" alt="multiple-instance echo diagram" height="600"/>

### Distributed echo server

The `GenEchoes` implementation actually supports distribution, too. To demonstrate that, an additional PBT stateful model is provided, `GenEchoesDistStatefulPBTTest`. This contains a model that tests the `GenEchoes` implementation on a distributed setting like the following:

<img src="./doc/DistributedEchoes.png" alt="distributed echo diagram" height="600"/>

All models but this last one are run in this repo's CI pipeline, which executes `mix test --exclude distributed`. Running this last distributed PBT model requires manually starting a few nodes in advance. These are the nodes where the test-generated instances of `GenEchoesSup` (and random number of `GenEchoes` children) will be started and interacted with. See below for more detailed instructions on how to run the distributed tests.

## Testing

In order to build this project, you need to clone or download it and then run `mix test --exclude distributed`.

### Distributed testing

Provided that the expected number of Elixir nodes are already up and reachable, the complete test suite can be run with `mix test`. The distributed PBT model can also be run by itself with `mix test --only distributed`.

The expected number of nodes is currently hardcoded on line 6 of file `gen_echoes_dist_stateful_pbt_test.exs`, as is the cookie to be used by the different nodes (and the testing node, too). You can configure these to your liking or else start up four Elixir nodes using:

```
iex --sname alice@localhost --cookie testcookie -S mix
iex --sname bob@localhost --cookie testcookie -S mix
iex --sname carol@localhost --cookie testcookie -S mix
iex --sname dan@localhost --cookie testcookie -S mix
```

and then running the distributed model, or the whole test suite.

### Coverage

Coverage analysis can be generated with `mix test --exclude distributed --cover` (or `mix test --cover`). An HTML report will be generated under a new `cover` folder.

### Code analysis

Static analysis of the source code can be performed by running several tools.

* [Credo](https://hexdocs.pm/credo/overview.html) is a static analysis tool that hints to refactoring opportunities, complex code fragments and common mistakes or dark patterns. It is already a dependency of this project, and can be executed via `mix credo`.

* [Dialyzer](http://erlang.org/doc/man/dialyzer.html) is a static analysis tool that identifies type errors, amongst others. The [dialyxir](https://github.com/jeremyjh/dialyxir) project, that provides an interface to dialyzer for Elixir projects, is already a dependency of this project, and can be executed via `mix dialyzer`.

  Bear in mind this process is very CPU-intensive and can take a while, especially the first time it is launched.

