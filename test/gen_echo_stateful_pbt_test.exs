defmodule GenEchoStatefulPBTTest do
  use ExUnit.Case
  use PropCheck
  use PropCheck.StateM
  doctest GenEcho
  doctest GenEchoSup

  property "supervised echo server should work according to its model at all times", [:verbose] do
    forall cmds <- commands(__MODULE__) do
      startup()
      {history, state, result} = run_commands(__MODULE__, cmds)
      cleanup(state)

      (result == :ok)
      |> aggregate(command_names(cmds))
      |> when_fail(
        IO.puts("""
        History: #{inspect(history)}
        State: #{inspect(state)}
        Result: #{inspect(result)}
        """)
      )
    end
  end

  ## Stateful model

  # Initial model internal state: we will use it to store the echo server identifier
  def initial_state(), do: :none

  # All possible commands to run against the SUT
  def command(_state) do
    frequency([
      {80, {:call, GenEcho, :echo, [message()]}},
      {20, {:call, __MODULE__, :kill, []}}
    ])
  end

  # Generators
  def message(), do: let(c <- non_empty(list(range(?a, ?z))), do: to_string(c))

  # Conditions needed for each command to be run
  def precondition(_state, {:call, _mod, _fun, _args}), do: true

  # Expected results from commands
  def postcondition(_state, {:call, GenEcho, :echo, [message]}, message), do: true
  def postcondition(_state, {:call, __MODULE__, :kill, []}, :ok), do: true
  def postcondition(_state, {:call, _mod, _fun, _args}, _res), do: false

  # Model next state after successful postconditions
  def next_state(state, _res, {:call, _mod, _fun, _args}), do: state

  # Setup
  def startup() do
    Process.flag(:trap_exit, true)
    {:ok, _pid} = GenEchoSup.start_link([])
    :ok
  end

  # Cleanup
  def cleanup(_state) do
    case Process.whereis(GenEchoSup) do
      nil ->
        :ok

      pid ->
        Process.exit(pid, :test_cleanup)
        Process.sleep(110)
    end
  end

  # Test utility functions
  def kill() do
    case Process.whereis(GenEcho) do
      nil ->
        :ok

      pid ->
        Process.exit(pid, :test)
        Process.sleep(10)
    end
  end
end
