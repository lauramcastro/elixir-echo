defmodule EchoStatefulPBTTest do
  use ExUnit.Case
  use PropCheck
  use PropCheck.StateM
  doctest Echo

  property "echo server should work according to its model at all times", [:verbose] do
    forall cmds <- commands(__MODULE__) do
      startup()
      {history, state, result} = run_commands(__MODULE__, cmds)
      cleanup(state)

      (result == :ok)
      |> aggregate(command_names(cmds))
      |> when_fail(
        IO.puts("""
        History: #{inspect(history)}
        State: #{inspect(state)}
        Result: #{inspect(result)}
        """)
      )
    end
  end

  ## Stateful model

  # Initial model internal state: we will use it to store the echo server identifier
  def initial_state(), do: :none

  # All possible commands to run against the SUT
  def command(state) do
    frequency([
      {20, {:call, Echo, :start, []}},
      {70, {:call, Echo, :echo, [state, message()]}},
      {10, {:call, Echo, :stop, [state]}}
    ])
  end

  # Generators
  def message(), do: let(c <- non_empty(list(range(?a, ?z))), do: to_string(c))

  # Conditions needed for each command to be run
  def precondition(:none, {:call, Echo, :stop, [_pid]}), do: false
  def precondition(:none, {:call, Echo, :echo, [_pid, _message]}), do: false
  def precondition(_state, {:call, _mod, _fun, _args}), do: true

  # Expected results from commands
  def postcondition(_state, {:call, Echo, :start, []}, res), do: is_pid(res)
  def postcondition(pid, {:call, Echo, :echo, [pid, message]}, message), do: true
  def postcondition(_state, {:call, Echo, :stop, [_pid]}, :ok), do: true
  def postcondition(_state, {:call, _mod, _fun, _args}, _res), do: false

  # Model next state after successful postconditions
  def next_state(_state, pid, {:call, Echo, :start, []}), do: pid
  def next_state(_state, _res, {:call, Echo, :stop, [_pid]}), do: :none
  def next_state(state, _res, {:call, _mod, _fun, _args}), do: state

  # Setup
  defp startup(), do: :ok

  # Cleanup
  defp cleanup(_state), do: :ok
end
