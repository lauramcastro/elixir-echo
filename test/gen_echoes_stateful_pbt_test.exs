defmodule GenEchoesStatefulPBTTest do
  use ExUnit.Case
  use PropCheck
  use PropCheck.StateM
  doctest GenEchoes
  doctest GenEchoesSup

  property "supervised multiple echo servers should all work according to their model at all times",
           [:verbose] do
    forall cmds <- commands(__MODULE__) do
      startup()
      {history, state, result} = run_commands(__MODULE__, cmds)
      cleanup(state)

      (result == :ok)
      |> aggregate(command_names(cmds))
      |> when_fail(
        IO.puts("""
        History: #{inspect(history)}
        State: #{inspect(state)}
        Result: #{inspect(result)}
        """)
      )
    end
  end

  ## Stateful model

  # Initial model internal state: we will use it to store the echo server identifiers
  def initial_state(), do: []

  # All possible commands to run against the SUT
  def command([]) do
    {:call, GenEchoesSup, :start_link, [pos_integer()]}
  end

  def command(state) do
    frequency([
      {70, {:call, GenEchoes, :echo, [oneof(state), message()]}},
      {30, {:call, __MODULE__, :kill, [oneof(state)]}}
    ])
  end

  # Generators
  def message(), do: let(c <- non_empty(list(range(?a, ?z))), do: to_string(c))

  # Conditions needed for each command to be run
  def precondition([], {:call, GenEchoesSup, :start_link, _args}), do: true
  def precondition([], {:call, GenEchoes, :echo, _args}), do: false
  def precondition([], {:call, __MODULE__, :kill, _args}), do: false
  def precondition(_state, {:call, GenEchoesSup, :start_link, _args}), do: false
  def precondition(_state, {:call, _mod, _fun, _args}), do: true

  # Expected results from commands
  def postcondition([], {:call, GenEchoesSup, :start_link, [_n]}, _res), do: true
  def postcondition(_state, {:call, GenEchoes, :echo, [s, message]}, {s, message}), do: true
  def postcondition(_state, {:call, __MODULE__, :kill, _args}, _res), do: true
  def postcondition(_state, {:call, _mod, _fun, _args}, _res), do: false

  # Model next state after successful postconditions
  def next_state([], _res, {:call, GenEchoesSup, :start_link, [n]}) do
    Enum.map(1..n, fn e -> String.to_atom("echo" <> to_string(e)) end)
  end

  def next_state(state, _res, {:call, _mod, _fun, _args}), do: state

  # Setup
  def startup() do
    Process.flag(:trap_exit, true)
    :ok
  end

  # Cleanup
  def cleanup(_state) do
    case Process.whereis(GenEchoesSup) do
      nil ->
        :ok

      pid ->
        Process.exit(pid, :test_cleanup)
        Process.sleep(110)
    end
  end

  # Test auxiliary functions
  def kill(server) do
    pid = Process.whereis(server)
    Process.exit(pid, :test)
    Process.sleep(10)
  end
end
