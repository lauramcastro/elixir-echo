defmodule GenEchoesDistStatefulPBTTest do
  use ExUnit.Case
  use PropCheck
  use PropCheck.StateM

  @nodes [{:alice, :localhost}, {:bob, :localhost}, {:carol, :localhost}, {:dan, :localhost}]
  @cookie :testcookie

  @tag distributed: true
  @tag timeout: :infinity
  property "supervised multiple echo servers distributed over several nodes should all work according to their model at all times",
           [:verbose, numtests: 1000] do
    # general setup
    {:ok, _pid} = Node.start(:testnode, :shortnames)
    true = Node.set_cookie(@cookie)
    for n <- @nodes, do: :pong = Node.ping(nodename(n))
    # now the real property
    forall cmds <- commands(__MODULE__) do
      startup()
      {history, state, result} = run_commands(__MODULE__, cmds)
      cleanup(state)

      (result == :ok)
      |> aggregate(command_names(cmds))
      |> when_fail(
        IO.puts("""
        History: #{inspect(history)}
        State: #{inspect(state)}
        Result: #{inspect(result)}
        """)
      )
    end
  end

  ## Stateful model

  # Initial model internal state: we will use it to store the echo server identifiers in each node
  def initial_state(), do: []

  # All possible commands to run against the SUT
  # TODO as exercise: add the stop_sup command!
  # TODO as exercise: make the GenEchoesSup able to dynamically change the number of supervised echo servers!
  def command([]) do
    {:call, __MODULE__, :start_sup, [oneof(@nodes), pos_integer()]}
  end

  def command(state) do
    frequency([
      {20, {:call, __MODULE__, :start_sup, [oneof(@nodes), pos_integer()]}},
      {50, {:call, __MODULE__, :echo, [oneof(state), message()]}},
      {30, {:call, __MODULE__, :kill, [oneof(state)]}}
    ])
  end

  # Generators
  def message(), do: let(c <- non_empty(list(range(?a, ?z))), do: to_string(c))

  # Conditions needed for each command to be run
  def precondition([], {:call, __MODULE__, :echo, _args}), do: false
  def precondition([], {:call, __MODULE__, :kill, _args}), do: false
  def precondition(_state, {:call, _mod, _fun, _args}), do: true

  # Expected results from commands
  def postcondition(_state, {:call, __MODULE__, :start_sup, _args}, true), do: true
  def postcondition(_state, {:call, __MODULE__, :echo, [_sn, message]}, {_s, message}), do: true
  def postcondition(_state, {:call, __MODULE__, :kill, _args}, true), do: true
  def postcondition(_state, {:call, _mod, _fun, _args}, _res), do: false

  # Model next state after successful postconditions
  def next_state(state, _res, {:call, __MODULE__, :start_sup, [where, howmany]}) do
    case Enum.filter(state, fn {_server, node} -> node == nodename(where) end) do
      [] -> Enum.map(1..howmany, fn i -> {servername(i), nodename(where)} end) ++ state
      _ -> state
    end
  end

  def next_state(state, _res, {:call, _mod, _fun, _args}), do: state

  defp nodename({name, host}), do: String.to_atom(to_string(name) <> "@" <> to_string(host))

  defp servername(n), do: String.to_atom("echo" <> to_string(n))

  # Setup
  def startup() do
    Process.flag(:trap_exit, true)
    # TODO: figure out how to do load app into slave node
    # {:ok, _node} = :slave.start_link(node, name)
    :ok
  end

  # Cleanup
  def cleanup(state) do
    # TODO: for {n, _servers} <- state, do: :ok = :slave.stop(n)
    true =
      state
      |> Enum.map(fn {_server, node} -> node end)
      |> Enum.uniq()
      |> Enum.map(fn node -> true = :rpc.block_call(node, GenEchoesSup, :stop, []) end)
      |> Enum.all?()

    :ok
  end

  # Test auxiliary functions
  def start_sup(node, n) do
    :rpc.block_call(nodename(node), GenEchoesSup, :start_link, [n])
    Process.sleep(10)
    true
  end

  def echo(echoserver, message) do
    GenEchoes.echo(echoserver, message)
  end

  def kill({server, node}) do
    # although GenServers can be messaged from anywere, they can only be killed locally
    true = :rpc.block_call(node, GenEchoes, :kill, [server])
    Process.sleep(10)
    true
  end
end
